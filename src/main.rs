struct Grid {
    linear_data: Vec<u32>,
    width: usize,
    height: usize,
}

impl Grid {
    fn new(linear_data: Vec<u32>, width: usize, height: usize) -> Self {
        Self {
            linear_data,
            width,
            height,
        }
    }

    fn get_row(&self, y: usize) -> Option<Vec<u32>> {
        let slice_start = y * self.width;
        let slice_end = slice_start + self.width;
        if slice_start >= self.linear_data.len() || slice_end > self.linear_data.len() {
            return None;
        }
        Some(self.linear_data[slice_start..slice_end].to_vec())
    }

    fn get_column(&self, x: usize) -> Option<Vec<u32>> {
        if x > self.width {
            return None;
        }

        Some(
            self.linear_data
                .iter()
                .skip(x)
                .step_by(self.width)
                .copied()
                .collect::<Vec<_>>(),
        )
    }

    fn get_point_by_index(&self, indx: usize) -> Option<(usize, usize)> {
        if indx > self.linear_data.len() {
            return None;
        }

        let x = indx % self.width;
        let y = indx / self.width;

        Some((x, y))
    }

    fn get_views_at_index(&self, indx: usize) -> Option<Vec<Vec<u32>>> {
        let (x, y) = self.get_point_by_index(indx)?;

        let mut left = self.get_row(y)?;
        let mut right = left.split_off(x);
        let mut top = self.get_column(x)?;
        let mut bottom = top.split_off(y);

        top.reverse();
        right.remove(0);
        left.reverse();
        bottom.remove(0);

        Some(vec![top, right, bottom, left])
    }
}

fn main() {
    let input = include_str!("../input.txt");
    println!("Solution A: {}", solve_a(input));
    println!("Solution B: {}", solve_b(input));
}

fn build_grid(input: &str) -> Grid {
    let y_size = input.lines().count();
    let x_size = input.lines().last().unwrap_or("").len();

    let mut linear_data = input.to_owned();
    linear_data.retain(|c| c.is_ascii_digit());
    let linear_data: Vec<_> = linear_data.chars().flat_map(|c| c.to_digit(10)).collect();

    Grid::new(linear_data, x_size, y_size)
}

fn solve_a(input: &str) -> u32 {
    let forest = build_grid(input);
    let mut visible = (forest.width * 2 + (forest.height - 2) * 2) as u32;
    for (indx, tree) in forest
        .linear_data
        .iter()
        .enumerate()
        .skip(forest.width)
        .take(forest.linear_data.len() - forest.width * 2)
    {
        if (indx + 1) % forest.width < 2 {
            continue;
        }

        if let Some((x, y)) = forest.get_point_by_index(indx) {
            let mut left = forest.get_row(y).unwrap();
            let mut right = left.split_off(x);
            right.remove(0);

            let mut top = forest.get_column(x).unwrap();
            let mut bottom = top.split_off(y);
            bottom.remove(0);

            for direction in [left, right, top, bottom] {
                if direction.iter().any(|t| t >= tree) {
                    continue;
                }

                visible += 1;
                break;
            }
        }
    }

    visible
}

fn solve_b(input: &str) -> u32 {
    let forest = build_grid(input);
    let high_score =
        forest
            .linear_data
            .iter()
            .enumerate()
            .fold(0, |hi_score: u32, (indx, tree)| {
                let mut score = 1;
                if let Some(views) = forest.get_views_at_index(indx) {
                    for view in views {
                        let view_score = match view.iter().position(|t| t >= tree) {
                            Some(distance) => distance + 1,
                            None => view.len(),
                        } as u32;

                        score *= view_score;
                    }
                }

                hi_score.max(score)
            });

    high_score
}

#[test]
fn test_solve_b() {
    let input = include_str!("../test.txt");
    assert_eq!(8, solve_b(input));
}

#[test]
fn test_solve_a() {
    let input = include_str!("../test.txt");
    assert_eq!(21, solve_a(input));
}
